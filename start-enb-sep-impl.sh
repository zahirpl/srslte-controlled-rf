#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
# IP address of the epc node interface towards the enb node(s)
EPC_ENB_IPADDR=$(getent hosts epc-link-1 | cut -s -d ' ' -f 1)

# IP address of the interface towards the epc node
HOSTNAME=$(hostname | cut -s -d '.' -f 1)
ENB_EPC_IPADDR=$(getent hosts $HOSTNAME-link-1 | cut -s -d ' ' -f 1)

echo  sudo srsenb /etc/srslte/enb.conf --enb.mme_addr $EPC_ENB_IPADDR --enb.gtp_bind_addr $ENB_EPC_IPADDR --enb.s1c_bind_addr $ENB_EPC_IPADDR | $MYPATH/log.sh
sudo srsenb /etc/srslte/enb.conf --enb.mme_addr $EPC_ENB_IPADDR --enb.gtp_bind_addr $ENB_EPC_IPADDR --enb.s1c_bind_addr $ENB_EPC_IPADDR

