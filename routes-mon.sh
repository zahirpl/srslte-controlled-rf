#!/usr/bin/env bash

# Keep trying to add the routes with routes-ue.sh every 15 seconds.
# This is because they cannot be added until the RAN tunnel exists and must be re-added
# after the RAN tunnel drops.

(
MYPATH=$(realpath $0 | xargs dirname)

while [[ 1 ]]; do
    sudo $MYPATH/routes-ue.sh
    if [[ $? == 0 ]]; then
        echo routes-ue.sh added routes | $MYPATH/log.sh
    fi
sleep 15
done
) 2> /dev/null > /tmp/routes-mon.log &

