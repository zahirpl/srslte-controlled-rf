#!/usr/bin/env bash

# Run appropriate route script

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)

if [[ $NODE_ID == "rue"* ]]; then
    SCRIPT=$MYPATH/routes-ue.sh
elif [[ $NODE_ID == "cndn" ]]; then
    SCRIPT=$MYPATH/routes-cndn.sh
elif [[ $NODE_ID == "epc" ]]; then
    SCRIPT=$MYPATH/routes-epc.sh
else
    echo "no routes to add on $NODE_ID"
    exit 0
fi

exec sudo $SCRIPT
