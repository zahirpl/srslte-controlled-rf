#!/usr/bin/env python
# srsLTE Controlled RF
"""Use this profile to intantiate an end-to-end LTE network in a controlled RF
environment (SDRs with wired connections) using srsLTE release 20.04.1.

The following nodes will be deployed:

* One or two Intel NUC5300/B210 w/ srsLTE eNodeBs with optionally co-located EPC services (`enb1`, 'enb2')
* Between one and four Intel NUC5300/B210 w/ srsLTE UE nodes (`rue1` ...`rue4`)
* An EPC compute node (optionally merged with 'enb1').
* Compute node acting as the NDN gateway connected to epc or enb1 (`cndn`) 
* Shadow compute nodes connected to UE nodes (`sue1` ...`sue4`)

Instructions:

The srsLTE will be automatically run on the UE nodes  (sue*), the eNodeB nodes (enb*) and the EPC node (epc).
The routes.sh script will be run on the NDN gateway node, cndn.
The route-mon.sh script will be run on the rue* nodes. This runs the routes-ue.sh script to ensure the
correct routes are set.

If the option to automatically start the software is not checked, do the following at startup.

On each of the nodes running srsLTE, rue*, enb* and epc, do the following:

```
cd /local/repository
./start-init.sh

```

To see if the srsLTE RAN connection is established, do the following ping test on rue* nodes:
...
ping 172.16.0.1
...


If srsLTE is not run automatically, once the srsLTE RAN connection is established, 
you can execute the "route"
helper script to enable end-to-end routing between `sue*` nodes and `cndn`.
This script can be safely run on any node, but only needs to run on
`cndn` and the UE nodes (`rue1` ... `ruen`).

```
/local/repository/routes.sh
```

After the routes are setup, you should be able to ping from `sue*` to `cndn`.

```
user@sue1> ping cndn
```

Note that IP traffic is masqueraded when going through the UE
nodes. So, e.g., traffic from `sue1` will appear to come from the `rue1`
RAN address (172.0.0.X) at `cndn`.  Another implication of this
masquerading is that you must generally initiate connections from the
UE side (shadow nodes).  However, port forwarding is setup for port 6363
(TCP and UDP) from the UE nodes to their associated shadow nodes.

"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.pg as pg

class GLOBALS(object):
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1"
    NUC_HWTYPE = "nuc5300"

# Get Context and Request objects.
pc = portal.Context()
request = pc.makeRequestRSpec()

# Define profile parameters
portal.context.defineParameter(
    "num_ues",
    "Number of UE nodes.",
    portal.ParameterType.INTEGER, 1, [1,2,3,4],
    "The selected number of UE nodes will be instantiated.  Each will have an RF link setup to an eNodeB. No more than two total can be connected to a single eNodeB due to RF attenuator matrix path limits."
)

portal.context.defineParameter(
    "startSrsLte",
    "Automatically start srsLTE software.",
    portal.ParameterType.BOOLEAN, True, 
    [True, False],
    "If True, the scripts to start srsLTE will be run on the eNodeB, EPC and the UE. The IP routes are configured between the nodes.",
    advanced=True
)

portal.context.defineParameter(
    "separateEPC",
    "Create a separate EPC node",
    portal.ParameterType.BOOLEAN, True, 
    [True, False],
    "If True, a separate EPC node is created. Otherwise, EPC is run on the enb1 node. Must be true if more than two UEs requiring 2 eNodeBs",
    advanced=True
)

portal.context.defineParameter(
    "cnodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of compute nodes attached to the eNB/core and UE nodes running NDN software.",
    advanced=True
)

portal.context.defineParameter(
    "epcnodetype",
    "EPC node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of EPC node attached to the eNB nodes. Use d430 if using traffic shaping",
    advanced=True
)

# Traffic shaping can only be used on nodes without 10Gb interfaces. e.g. d430
# srsLTe is built to use AVX instructions e.g. d710 or d430.
portal.context.defineParameter(
    "simulateRemoteGateway",
    "Simulate remote gateway (cndn) using traffic shaping",
    portal.ParameterType.BOOLEAN, False, 
    [True, False],
    "If True, the link between the epc node and the cndn node has traffic shaping to simulate the increased latency, lower bandwidth and possible packet loss seen with remote gateways",
    advanced=True
)

portal.context.defineParameter(
    "packetLoss",
    "If simulate remote gateway is true, Packet loss can be specified 0.0 <= N < 1.0",
    portal.ParameterType.LOSSRATE, 0.0, 
    advanced=True
)

portal.context.defineParameter(
    "latency",
    "If simulate remote gateway is true, latency can be specified in ms",
    portal.ParameterType.LATENCY, 10.0, 
    advanced=True
)

portal.context.defineParameter(
    "bandwidth",
    "If simulate remote gateway is true, bandwidth can be specified in kbps",
    portal.ParameterType.BANDWIDTH, 10000.0, 
    advanced=True
)

# Bind parameters
params = portal.context.bindParameters()

# Bind parameters
params = portal.context.bindParameters()

if not params.separateEPC and params.num_ues > 2:
  portal.context.reportError(portal.ParameterError( "A separate EPC must be specified for more than 2 UEs", 
      ['num_ues'] ), 
          immediate=True )

# Add a NUC eNB node 1
enb1 = request.RawPC("enb1")
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.SRSLTE_IMG
enb1.Desire("rf-controlled", 1)
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
if params.startSrsLte:
  enb1.addService(rspec.Execute(shell="bash", command="/local/repository/start-impl.sh"))
if params.separateEPC:
  lan = request.LAN()
  enb1_link = enb1.addInterface("epcif")
  lan.addInterface(enb1_link)

if params.num_ues > 2:
  # Add a NUC eNB node 2
  enb2 = request.RawPC("enb2")
  enb2.hardware_type = GLOBALS.NUC_HWTYPE
  enb2.disk_image = GLOBALS.SRSLTE_IMG
  enb2.Desire("rf-controlled", 1)
  enb2.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
  enb2.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
  if params.startSrsLte:
    enb2.addService(rspec.Execute(shell="bash", command="/local/repository/start-impl.sh"))
  enb2_link = enb2.addInterface("epcif")
  lan.addInterface(enb2_link)

if params.separateEPC:
  # Add EPC server, core network side
  epc = request.RawPC("epc") 
  epc.hardware_type = params.epcnodetype
  epc.disk_image = GLOBALS.SRSLTE_IMG
  epc.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
  epc.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
  if params.startSrsLte:
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/start-impl.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/routes.sh"))
  
  epc_link = epc.addInterface("enbif")
  lan.addInterface(epc_link)

# Add NDN server, core network side
cndn = request.RawPC("cndn") 
cndn.hardware_type = params.cnodetype
cndn.disk_image = GLOBALS.UBUNTU_1804_IMG
if params.separateEPC:
  cndn_link = request.Link("cndn-link", members=[cndn, epc])
else:
  cndn_link = request.Link("cndn-link", members=[cndn, enb1])
if params.simulateRemoteGateway:
  # Bandwidth is in kbps
  cndn_link.bandwidth = params.bandwidth
  # Latency is in ms
  cndn_link.latency = params.latency
  # Packet loss is a number 0.0 <= loss <= 1.0
  cndn_link.plr = params.packetLoss
if params.startSrsLte:
  cndn.addService(rspec.Execute(shell="bash", command="/local/repository/routes.sh"))

# Add the requested number of UE nodes (attached to the eNodeB via
# controlled RF links).  Each has its own shadow compute node.
for id in range(1, params.num_ues + 1):
    # Add a NUC node
    rue = request.RawPC("rue%d" % id)
    rue.hardware_type = GLOBALS.NUC_HWTYPE
    rue.disk_image = GLOBALS.SRSLTE_IMG
    rue.Desire("rf-controlled", 1)
    rue.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
    if params.startSrsLte:
      rue.addService(rspec.Execute(shell="bash", command="/local/repository/start-impl.sh"))
      rue.addService(rspec.Execute(shell="bash", command="/local/repository/routes-mon.sh"))

    if id < 3:
        enb = enb1
        enb_id = 1
    else:
        enb = enb2
        enb_id = 2

    # Create the RF link between this UE and the eNodeB
    rflink = request.RFLink("rflink%d" % id)
    enb1_rf_if = enb.addInterface("enb%d_rf_if%d" % (enb_id, id))
    rue_rf_if = rue.addInterface("rue%d_rf_if" % id)
    rflink.addInterface(enb1_rf_if)
    rflink.addInterface(rue_rf_if)

    # Add UE shadow node
    sue = request.RawPC("sue%d" % id)
    sue.hardware_type = params.cnodetype
    sue.disk_image = GLOBALS.UBUNTU_1804_IMG
    sue_link = request.Link("sue%d-link" % id, members=[sue, rue])
    
    # We need a link to talk to the remote file system, so make an interface.
    sue_iface = sue.addInterface()

    # The remote file system is represented by special node.
    sue_fsnode = request.RemoteBlockstore("sue%d-fsnode" % id, "/mydata")
    # This URN is displayed in the web interfaace for your dataset.
    # VM Dataset
    sue_fsnode.dataset = "urn:publicid:IDN+emulab.net:verinet-5g+ltdataset+virtual-machines"
    #
    # The "rwclone" attribute allows you to map a writable copy of the
    # indicated SAN-based dataset. In this way, multiple nodes can map
    # the same dataset simultaneously. In many situations, this is more
    # useful than a "readonly" mapping. For example, a dataset
    # containing a Linux source tree could be mapped into multiple
    # nodes, each of which could do its own independent,
    # non-conflicting configure and build in their respective copies.
    # Currently, rwclones are "ephemeral" in that any changes made are
    # lost when the experiment mapping the clone is terminated.
    #
    sue_fsnode.rwclone = True

    #
    # The "readonly" attribute, like the rwclone attribute, allows you to
    # map a dataset onto multiple nodes simultaneously. But with readonly,
    # those mappings will only allow read access (duh!) and any filesystem
    # (/mydata in this example) will thus be mounted read-only. Currently,
    # readonly mappings are implemented as clones that are exported
    # allowing just read access, so there are minimal efficiency reasons to
    # use a readonly mapping rather than a clone. The main reason to use a
    # readonly mapping is to avoid a situation in which you forget that
    # changes to a clone dataset are ephemeral, and then lose some
    # important changes when you terminate the experiment.
    #
    #sue_fsnode.readonly = True

    # Now we add the link between the node and the special node
    sue_fslink = request.Link("sue%d-fslink" % id)
    sue_fslink.addInterface(sue_iface)
    sue_fslink.addInterface(sue_fsnode.interface)

    # Special attributes for this link that we must use.
    sue_fslink.best_effort = True
    sue_fslink.vlan_tagging = True

    #sue.addService(rspec.Execute(shell="bash", command="/local/repository/install-kvm.sh"))
    #sue.addService(rspec.Execute(shell="bash", command="/local/repository/add-vm-bridge.sh"))

pc.printRequestRSpec(request)
