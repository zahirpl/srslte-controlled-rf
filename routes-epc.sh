#!/usr/bin/env bash

# To be run on the `epc` compute node in the "scenario 2" profile.
# This adds a NAT rule so that UE traffic can traverse the Internet.

MYPATH=$(realpath $0 | xargs dirname)
CTRLADDR=$(cat /var/emulab/boot/controlif)

ERR=0

sudo /sbin/iptables -t nat -A POSTROUTING -o $CTRLADDR -j MASQUERADE || ERR=1

if [[ $ERR == 1 ]]; then
	echo $(basename $0) "Failed to add routes!"
else
	echo $(basename $0) "Finished adding routes."
fi | $MYPATH/log.sh

exit $ERR
