#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)
NUM=${NODE_ID:3}
HEXID=$(printf "%02x" $NUM)
echo "sudo srsue /etc/srslte/ue.conf --usim.imsi=0010101234567$HEXID" | $MYPATH/log.sh
sudo srsue /etc/srslte/ue.conf --usim.imsi=0010101234567$HEXID
